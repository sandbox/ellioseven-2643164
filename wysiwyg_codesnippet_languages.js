(function ($, Drupal) {

  Drupal.behaviors.wysiwyg_codesnippet_languages = {

    /**
     * Get default and extend languages.
     *
     * @param source
     * @returns {}
     */
    getLangs: function(source) {

      // Check for source.
      if (!source) { return {} }
      var langs = {};

      // Format source values into array
      if (source.split("\n").length) {
        source = source.split("\n")
      } else {
        source = [source];
      }

      // Loop through each source value and append key value pair to object.
      $.each(source, function(i){
        var parts = source[i].split(":");
        langs[parts[0]] = parts[1];
      });

      return langs;

    },

    /**
     * Constructor.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {

      var behavior = this;

      if (typeof CKEDITOR !== 'undefined') {

        // Define language defaults from highlight JS common.
        // @url https://highlightjs.org/download/
        lang_default = {
          apache: 'Apache',
          bash: 'Bash',
          coffeescript: 'CoffeeScript',
          cpp: 'C++',
          cs: 'C#',
          css: 'CSS',
          diff: 'Diff',
          html: 'HTML',
          http: 'HTTP',
          ini: 'INI',
          java: 'Java',
          javascript: 'JavaScript',
          json: 'JSON',
          makefile: 'Makefile',
          markdown: 'Markdown',
          nginx: 'Nginx',
          objectivec: 'Objective-C',
          perl: 'Perl',
          php: 'PHP',
          python: 'Python',
          ruby: 'Ruby',
          sql: 'SQL',
          vbscript: 'VBScript',
          xhtml: 'XHTML',
          xml: 'XML'
        };

        // Set codeSnippet languages.
        // @url http://docs.ckeditor.com/#!/guide/dev_codesnippet
        CKEDITOR.config.codeSnippet_languages = $.extend(
          lang_default,
          behavior.getLangs(Drupal.settings.wysiwyg.codeSnippet_languages)
        );

      }
    }

  }

})(jQuery, Drupal);